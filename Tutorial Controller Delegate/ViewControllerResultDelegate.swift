//
//  ViewControllerResultDelegate.swift
//  Tutorial Controller Delegate
//
//  Created by Tommy Ryanto on 09/11/20.
//

import Foundation

protocol ViewControllerResultDelegate: class {
    
    func onRetrieveResult(requestCode: String?, isSuccess : Bool, result: [String: String])
    
}
