//
//  CustomTextField.swift
//  Tutorial Controller Delegate
//
//  Created by Tommy Ryanto on 11/11/20.
//

import UIKit

class CustomTextField: UIView {

    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initView()
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        initView()
    }
    
    public func initView() {
        Bundle.main.loadNibNamed("CustomTextField", owner: self, options: nil)
        addSubview(contentView)
        self.errorLabel.isHidden = true
    }
    
    public func secureTextValue() {
        self.textField.isSecureTextEntry = true
    }
    
    public func setHeaderText(text: String) {
        self.headerLabel.text = text
    }
    
    public func isNameCorrect() -> Bool {
        guard let name = self.textField.text else { return false }
        let valid = name.count > 5
        if !valid {
            errorLabel.text = "Name not valid, must be 5 characters or longer"
            errorLabel.textColor = .red
        }
        errorLabel.isHidden = valid
        return valid
    }
    
    public func isEmailCorrect() -> Bool {
        guard let email = self.textField.text else { return false }
        let valid = email.hasSuffix(".com") && email.contains("@")
        if !valid {
            errorLabel.text = "Email not valid, must be ended by .com and contain @"
            errorLabel.textColor = .red
        }
        errorLabel.isHidden = valid
        return valid
    }
    
    public func getValue() -> String {
        return self.textField.text!
    }
    
    public func passwordValid() -> Bool {
        guard let password = self.textField.text else { return false }
        let valid = password.count >= 8
        if !valid {
            self.errorLabel.text = "Password must be 8 characters or longer"
            self.errorLabel.textColor = .red
        }
        self.errorLabel.isHidden = valid
        return valid
    }
    
    
}
