//
//  TextViewer.swift
//  Tutorial Controller Delegate
//
//  Created by Tommy Ryanto on 12/11/20.
//

import UIKit

class TextViewer: UIView {
    
    @IBOutlet weak var textHeader: UILabel!
    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet var contentView: UIView!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initView()
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        initView()
    }
    
    func initView() {
        Bundle.main.loadNibNamed("TextViewer", owner: self, options: nil)
        addSubview(contentView)
    }

    public func setHeaderText(text: String) {
        self.textHeader.text = text
    }
    
    public func setDataText(text: String) {
        self.dataLabel.text = text
    }
    
}
