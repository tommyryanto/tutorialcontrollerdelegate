//
//  Onboarding.swift
//  Tutorial Controller Delegate
//
//  Created by Tommy Ryanto on 10/11/20.
//

import Foundation

struct Onboarding {
    var name: String
    var email: String
    var password: String
}
