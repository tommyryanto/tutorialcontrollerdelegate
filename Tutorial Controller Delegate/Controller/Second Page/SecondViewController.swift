//
//  SecondViewController.swift
//  Tutorial Controller Delegate
//
//  Created by Tommy Ryanto on 04/11/20.
//

import UIKit

class SecondViewController: UIViewController {
    
    //var parentView: SecondView!
    var viewControllerResultDelegate: ViewControllerResultDelegate?
    var controllerTag: String?
    @IBOutlet weak var passwordTextField: CustomTextField!
    @IBOutlet weak var confirmPasswordTextField: CustomTextField!
    
    @IBAction func nextBtn(_ sender: Any) {
        if passwordTextField.passwordValid() && passwordMatches() {
            let result =  [
                "password": passwordTextField.getValue()
            ]
            viewControllerResultDelegate?.onRetrieveResult(requestCode: self.controllerTag, isSuccess: true, result: result)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //showView()
        initForm()
    }
    
    private func initForm() {
        passwordTextField.setHeaderText(text: "Password")
        passwordTextField.secureTextValue()
        confirmPasswordTextField.setHeaderText(text: "Confirm Password")
        confirmPasswordTextField.secureTextValue()
    }
    
    private func passwordMatches() -> Bool {
        return passwordTextField.getValue() == confirmPasswordTextField.getValue()
    }
    
}

/*func showView() {
    parentView = SecondView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
    self.view.addSubview(parentView)
    parentView.translatesAutoresizingMaskIntoConstraints = false
    self.view.addConstraint(NSLayoutConstraint(item: self.view!, attribute: .top, relatedBy: .equal, toItem: parentView, attribute: .top, multiplier: 1, constant: 0))
    self.view.addConstraint(NSLayoutConstraint(item: self.view!, attribute: .left, relatedBy: .equal, toItem: parentView, attribute: .left, multiplier: 1, constant: 0))
    self.view.addConstraint(NSLayoutConstraint(item: self.view!, attribute: .right, relatedBy: .equal, toItem: parentView, attribute: .right, multiplier: 1, constant: 0))
    self.view.addConstraint(NSLayoutConstraint(item: self.view!, attribute: .bottom, relatedBy: .equal, toItem: parentView, attribute: .bottom, multiplier: 1, constant: 0))
    parentView.nextButton.addTarget(self, action: #selector(goToNextView), for: .touchUpInside)
}

@objc func goToNextView() {
    if !parentView.passwordTextField.text!.isEmpty && parentView.passwordTextField.text == parentView.confirmPasswordTextField.text {
        self.performSegue(withIdentifier: "goToThirdPage", sender: self)
    } else  {
        let alert = UIAlertController(title: "Wrong Password Format", message: "Fill the password and make sure both passwordf and confirm constain the same", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
}*/
