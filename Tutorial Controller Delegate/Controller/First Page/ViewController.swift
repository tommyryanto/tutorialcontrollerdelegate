//
//  ViewController.swift
//  Tutorial Controller Delegate
//
//  Created by Tommy Ryanto on 04/11/20.
//

import UIKit

class ViewController: UIViewController {
    
    private static let REQUEST_SECOND_VIEW = "SECOND_VIEW"
    private static let REQUEST_THIRD_VIEW = "THIRD_VIEW"
    var onboarding = [String : Any]()
    
    @IBOutlet weak var nameTextViewer: TextViewer!
    @IBOutlet weak var emailTextViewer: TextViewer!
    @IBOutlet weak var passwordTextViewer: TextViewer!
    //var parentView: FirstView!
    @IBOutlet weak var nameTextField: CustomTextField!
    @IBOutlet weak var emailTextField: CustomTextField!
    
    @IBAction func nextButton(_ sender: Any) {
        let valid = self.nameTextField.isNameCorrect() && self.emailTextField.isEmailCorrect()
        if valid {
            let result = [
                "name": nameTextField.getValue(),
                "email": emailTextField.getValue()
            ]
            self.onboarding["firstView"] = result
            
            startSecondPage()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "Onboarding"
        initForm()
    }
    
    private func initForm() {
        nameTextViewer.isHidden = true
        emailTextViewer.isHidden = true
        passwordTextViewer.isHidden = true
        emailTextField.setHeaderText(text: "Email Address")
        nameTextField.setHeaderText(text: "Name")
    }
    
    /*private func nameAndEmailCorrect() -> Bool {
        /*guard let name = nameTextField.text else { return false }
        guard let email = emailTextField.text else { return false }
        if name.count > 5  {
            if email.hasSuffix(".com") && email.contains("@") {
                return true
            }
        }*/
        return false
    }*/

}

// MARK: View Controller Result Delegate
extension ViewController: ViewControllerResultDelegate {
    func onRetrieveResult(requestCode: String?, isSuccess: Bool, result: [String : String]) {
        if isSuccess {
            if requestCode == ViewController.REQUEST_SECOND_VIEW {
                onSecondViewControllerResult(result: result)
            } else if requestCode == ViewController.REQUEST_THIRD_VIEW {
                onThirdViewControllerResult(result: result)
            }
        }
    }
    
    private func onSecondViewControllerResult(result: [String: String]) {
        self.onboarding["secondView"] = result
        self.navigationController?.popViewController(animated: false)
        
        startThirdPage()
    }
    
    private func onThirdViewControllerResult(result: [String: String]) {
        self.onboarding["thirdView"] = result
        self.navigationController?.popViewController(animated: false)
        
        configureTextViewer()
    }
    
    private func configureTextViewer() {
        
        print(self.onboarding)
        let firstView = onboarding["firstView"] as! [String: String]
        let secondView = onboarding["secondView"] as! [String: String]
        let name = firstView["name"]!
        let email = firstView["email"]!
        let password = secondView["password"]!
        
        nameTextViewer.isHidden = false
        emailTextViewer.isHidden = false
        passwordTextViewer.isHidden = false
        
        nameTextViewer.setHeaderText(text: "Name")
        emailTextViewer.setHeaderText(text: "Email")
        passwordTextViewer.setHeaderText(text: "Password")
        
        nameTextViewer.setDataText(text: name)
        emailTextViewer.setDataText(text: email)
        passwordTextViewer.setDataText(text: password)
    }
    
    private func startSecondPage() {
        let controller = SecondViewController(nibName: "SecondView", bundle: nil)
        controller.viewControllerResultDelegate = self
        controller.controllerTag = ViewController.REQUEST_SECOND_VIEW
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func startThirdPage() {
        let controller = ThirdViewController(nibName: "ThirdView", bundle: nil)
        controller.viewControllerResultDelegate = self
        controller.controllerTag = ViewController.REQUEST_THIRD_VIEW
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}





/*private func showView() {
    parentView = FirstView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
    self.view.addSubview(parentView)
    parentView.translatesAutoresizingMaskIntoConstraints = false
    self.view.addConstraint(NSLayoutConstraint(item: self.view!, attribute: .top, relatedBy: .equal, toItem: parentView, attribute: .top, multiplier: 1, constant: 0))
    self.view.addConstraint(NSLayoutConstraint(item: self.view!, attribute: .left, relatedBy: .equal, toItem: parentView, attribute: .left, multiplier: 1, constant: 0))
    self.view.addConstraint(NSLayoutConstraint(item: self.view!, attribute: .right, relatedBy: .equal, toItem: parentView, attribute: .right, multiplier: 1, constant: 0))
    self.view.addConstraint(NSLayoutConstraint(item: self.view!, attribute: .bottom, relatedBy: .equal, toItem: parentView, attribute: .bottom, multiplier: 1, constant: 0))
    parentView.nextButton.addTarget(self, action: #selector(goToNextView), for: .touchUpInside)
}

@objc func goToNextView() {
    if !parentView.nameTextField.text!.isEmpty && !parentView.emailTextField.text!.isEmpty {
        self.performSegue(withIdentifier: "goToSecondPage", sender: self)
    } else {
        print("name or email empty")
        let alert = UIAlertController(title: "Name and email can't be empty!", message: "Please fill your name and email", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
}*/
