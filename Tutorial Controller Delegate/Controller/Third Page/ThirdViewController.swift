//
//  ThirdViewController.swift
//  Tutorial Controller Delegate
//
//  Created by Tommy Ryanto on 04/11/20.
//

import UIKit

class ThirdViewController: UIViewController {
    
    //var parentView: ThirdView!
    var viewControllerResultDelegate: ViewControllerResultDelegate?
    var controllerTag: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //showView()
        Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { (timer) in
            self.moveToFirst()
        }
    }
    
    @objc func moveToFirst() {
        viewControllerResultDelegate?.onRetrieveResult(requestCode: controllerTag, isSuccess: true, result: [:])
    }
    
}

/*
 func showView() {
     parentView = ThirdView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
     self.view.addSubview(parentView)
     parentView.translatesAutoresizingMaskIntoConstraints = false
     self.view.addConstraint(NSLayoutConstraint(item: self.view!, attribute: .top, relatedBy: .equal, toItem: parentView, attribute: .top, multiplier: 1, constant: 0))
     self.view.addConstraint(NSLayoutConstraint(item: self.view!, attribute: .left, relatedBy: .equal, toItem: parentView, attribute: .left, multiplier: 1, constant: 0))
     self.view.addConstraint(NSLayoutConstraint(item: self.view!, attribute: .right, relatedBy: .equal, toItem: parentView, attribute: .right, multiplier: 1, constant: 0))
     self.view.addConstraint(NSLayoutConstraint(item: self.view!, attribute: .bottom, relatedBy: .equal, toItem: parentView, attribute: .bottom, multiplier: 1, constant: 0))
     //parentView.nextButton.addTarget(self, action: #selector(goToNextView), for: .touchUpInside)
 }
 */
